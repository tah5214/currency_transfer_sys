from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField,SelectField, TextAreaField, FloatField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User, TransferRequest

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class AddFundsForm(FlaskForm):
    amount = FloatField('Funds Amount', validators=[DataRequired()])
    submit = SubmitField('Submit Amount', validators=[DataRequired()])

class WithdrawForm(FlaskForm):
    amount = FloatField('Funds Amount', validators =[DataRequired()])
    submit = SubmitField('Submit Amount', validators=[DataRequired()])

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    country = SelectField('Country', choices=[("UK","UK"),("US","US")], default=2)
    submit = SubmitField('Register')

    def validate_username(self,username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self,email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class IssueForm(FlaskForm):
    subject = StringField('Title of issue', validators=[DataRequired()])
    body = TextAreaField('Report your issue', validators=[DataRequired()])
    submit = SubmitField('Send Issue Report')

class ContactRequestForm(FlaskForm):
    submit = SubmitField('Add Contact')

class TransferForm(FlaskForm):
    amount = StringField('Sender Amount', validators=[DataRequired()])
    src_country_currency = SelectField('Country', choices=[("UK", "UK"), ("US", "US")],validators=[DataRequired()])
    converted = StringField('Converted Funds')
    submit = SubmitField('Submit Amount', validators=[DataRequired()])
    
    def validate_receiver(self, receiver_username):
        user = TransferRequest.query.filter_by(receiver_username = receiver_username.data).first()
        if user is None:
            raise ValidationError('Please use a valid user')

class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')
    
class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')