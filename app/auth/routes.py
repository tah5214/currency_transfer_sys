from flask import request,escape,url_for,render_template,flash,redirect
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from app.models import User, \
                       ContactRequest, \
                       IssuePost, \
                       TransferRequest
from app import db
from app.auth import bp
from app.auth.forms import LoginForm, \
                      RegistrationForm, \
                      IssueForm, \
                      ContactRequestForm, \
                      TransferForm, \
                      AddFundsForm, \
                      WithdrawForm, \
                      ResetPasswordRequestForm, \
                      ResetPasswordForm
                      
from app.auth.email import send_password_reset_email


@bp.route('/')
@bp.route('/index')
@login_required
def index():
    transactions = TransferRequest.query.filter((TransferRequest.receiver_username == current_user.username) | \
                                                (TransferRequest.sender_username == current_user.username))
    return render_template('index.html', title='Home Page', transfers=transactions)

@bp.route('/about')
def about():
    return render_template('about.html')

@bp.route('/register', methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('auth.index'))
    form = RegistrationForm(request.form)
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data,country=form.country.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html',title='Register',form=form)

@bp.route('/login',methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('auth.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if (user is None) or (not user.check_password(form.password.data)):
            flash('Invalid username or password.')
            return redirect(url_for('auth.login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('auth.index')
        return redirect(next_page)
        #flash('Login requested for the user {}, remember_me={}'.format(
            #form.username.data,form.remember_me.data))
    return render_template('auth/login.html',title='Sign In', form=form)

@bp.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('auth.index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_password_request.html', form=form)
    
@bp.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('auth.index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('auth.index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_password.html', form=form)

@login_required
@bp.route('/contacts')
def contacts():
    return render_template('contacts.html')

@login_required
@bp.route('/add_contact',methods=['GET','POST'])
def add_contact():
    target_user = User.query.filter_by(username=request.form['add_contact']).first()
    if target_user is None:
        flash('That user does not exist.')
        return redirect(url_for('auth.contacts'))
    else:
        req = ContactRequest(sender_id=current_user.id,
                             sender_username=User.query.filter_by(id=current_user.id).first_or_404().username,
                             receiver_id=target_user.id,
                             receiver_username=User.query.filter_by(id=target_user.id).first_or_404().username)
        db.session.add(req)
        db.session.commit()
        flash('Your request has been sent.')
        return redirect(url_for('auth.contacts'))

@login_required
@bp.route('/accept_request', methods=['GET', 'POST'])
def accept_request():
    request_sender = User.query.filter_by(username=request.form.get('sender')).first_or_404()
    current_user.add_contact(request_sender) # accept it on current_user's end
    request_sender.add_contact(current_user) # AND ACCEPT from OTHER USER'S END TOO
    flash('You have added {} as a contact.'.format(request_sender.username))
    db.session.delete(ContactRequest.query.filter_by(sender_username=request_sender.username, \
                      receiver_username=current_user.username).first_or_404())
    db.session.commit()
    return redirect(url_for('auth.contacts'))

@login_required
@bp.route('/reject_request', methods=['GET', 'POST'])
def reject_request():
    request_sender = User.query.filter_by(username=request.form.get('sender')).first_or_404()
    flash('Contact request denied.'.format(request_sender.username))
    db.session.delete(ContactRequest.query.filter_by(sender_username=request_sender.username, \
                      receiver_username=current_user.username).first_or_404())
    db.session.commit()
    return redirect(url_for('auth.contacts'))

@login_required
@bp.route('/cancel_request', methods=['GET', 'POST'])
def cancel_request():
    request_receiver = User.query.filter_by(username=request.form.get('cancel_target')).first_or_404()
    flash('Contact request cancelled.')
    db.session.delete(ContactRequest.query.filter_by(sender_username=current_user.username, \
                      receiver_username=request_receiver.username).first_or_404())
    db.session.commit()
    return redirect(url_for('auth.contacts'))

@login_required
@bp.route('/remove_contact', methods=['GET', 'POST'])
def remove_contact():
    remove_target = User.query.filter_by(username=request.form.get('target')).first_or_404()
    flash('You have removed {} as a contact.'.format(remove_target.username))
    current_user.del_contact(remove_target)
    remove_target.del_contact(current_user)
    return redirect(url_for('auth.contacts'))

@login_required
@bp.route('/report_issue', methods=['GET', 'POST'])
def report_issue():
    form = IssueForm(request.form)
    if form.validate_on_submit():
        issue = IssuePost(user_id=current_user.id,
                          subject=form.subject.data,
                          body=form.body.data)
        db.session.add(issue)
        db.session.commit()
        flash('Your issue has been submitted. ID: #{}'.format(issue.id))
        return redirect(url_for('auth.index'))
    return render_template('report_issue.html', title='Report Issue', form=form)

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@login_required
@bp.route('/add_funds', methods=['GET', 'POST'])
def add_funds():
    form = AddFundsForm()
    if form.validate_on_submit():
        _amt = float(str(round(float(request.form.get('amount')), 2)))
        current_user.add_money(_amt)
        flash('Funds have been added')
        return redirect(url_for('auth.index'))
    return render_template('add_funds.html', title='Add Funds', form=form)

@login_required
@bp.route('/withdraw_funds', methods=['GET', 'POST'])
def withdraw_funds():
    form = WithdrawForm()
    if form.validate_on_submit():
        _amt = float(str(round(float(request.form.get('amount')), 2)))
        if current_user.can_transfer(_amt):
            current_user.withdraw_money(_amt)
            flash('Funds have been withdrawn')
            return redirect(url_for('auth.index'))
        else:
            flash("You don't have enough funds to withdraw that much.")
    return render_template('withdraw_funds.html', title='Withdraw Funds', form=form)

@bp.route('/transfer',methods=['GET','POST'])
@login_required
def transfer():
    _recipient = User.query.filter_by(username=request.form.get('target')).first()
    if _recipient is None:
        _recipient = User.query.filter_by(username=request.form.get('target_username')).first_or_404()
    form = TransferForm()
    form.src_country_currency.data = current_user.country
    if form.converted.data is "":
        form.converted.data = 0.0
    if form.validate_on_submit():
        _amt = float(str(round(float(request.form.get('amount')), 2)))
        _amt_wfee = _amt + (_amt*0.015)
        if form.converted.data == '0.0' and form.amount.data != "0":
            if form.src_country_currency.data == 'US' and _recipient.country == 'UK':
                form.converted.data = str(round((float(form.amount.data) * 0.76),2))
            if form.src_country_currency.data == 'UK' and _recipient.country == 'US':
                form.converted.data = str(round((float(form.amount.data) * 1.31),2))
            elif form.src_country_currency.data == _recipient.country:
                form.converted.data = form.amount.data
            return render_template('transfer.html', form=form, recipient=_recipient)

        elif current_user.can_transfer(_amt_wfee) and form.converted.data != '0.0':
            tr = TransferRequest(sender_username=current_user.username, \
                                 sender_id=current_user.id, \
                                 receiver_username=_recipient.username, \
                                 transfer_amount=form.converted.data)
            db.session.add(tr) 
            flash('Money sent')
            current_user.withdraw_money(_amt_wfee)
            _recipient.add_money(float(form.converted.data))
            db.session.commit()
            print(tr)
            return redirect(url_for('auth.index'))
        else:
            flash("You don't have enough funds to transfer that much.")
    return render_template('transfer.html',form=form, recipient=_recipient)

@login_required
@bp.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user_profile.html',user=user)
