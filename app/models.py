from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.orm import composite
from datetime import datetime
from flask_login import UserMixin
from app import db, login
from time import time
import jwt
from flask import current_app

#Association Table
contacts = db.Table('contacts',
                    db.Column('owner_id', db.Integer, db.ForeignKey('user.id')),
                    db.Column('contact_id',db.Integer, db.ForeignKey('user.id')))

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique = True)
    country = db.Column(db.String(120) , index=True)
    balance = db.Column(db.Float(), index=True, default=0)
    password_hash = db.Column(db.String(128))
    issue_posts = db.relationship('IssuePost',backref='author',lazy='dynamic')
    my_contacts = db.relationship(
        'User',secondary=contacts,
        primaryjoin=(contacts.c.owner_id == id),
        secondaryjoin=(contacts.c.contact_id == id),
        backref=db.backref('contacts', lazy='dynamic'),lazy='dynamic')
    transfer_requests = db.relationship('TransferRequest',backref='sender', lazy='dynamic')
    contact_req_sent = db.relationship('ContactRequest',
                                        foreign_keys='ContactRequest.sender_id',
                                        backref='sender',lazy='dynamic' )

    contact_req_recv = db.relationship('ContactRequest',
                                      foreign_keys='ContactRequest.receiver_id',
                                      backref='receiver',lazy='dynamic')
    
    
    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def add_contact(self, user):
        if not self.is_contact(user) and not user is None:
            self.my_contacts.append(user)
            db.session.commit()

    def del_contact(self, user):
        if self.is_contact(user) and not user is None:
            self.my_contacts.remove(user)
            db.session.commit()

    def is_contact(self, user):
        return self.my_contacts.filter(contacts.c.contact_id == user.id).count() > 0
            
    def add_money(self, amount):
        self.balance += amount
        db.session.commit()
        return
    
    def withdraw_money(self, amount):
        self.balance -= amount
        db.session.commit()
        return
    
    def can_transfer(self, amount):
        return self.balance >= amount
    
    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


class TransferRequest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    sender_username = db.Column(db.String(64))
    transfer_amount = db.Column(db.Integer)
    receiver_username = db.Column(db.String(64))##Should be indexable!!

    #todo_ <sender currency type _ receiver currency type transaction amount in DOLLARS

    def __repr__(self):
        return '<TransferRequest from {} (id {}) to {}. sender sent {}.>'.format(
            self.sender_username,
            self.sender_id,
            self.receiver_username,
            self.transfer_amount)

class IssuePost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    subject = db.Column(db.String(64))
    body = db.Column(db.String(140))
    status = db.Column(db.String(64), default="Open")
    responder_username = db.Column(db.String(64), default="N/A")

    def __repr__(self):
        return '<IssuePost {}>'.format(self.body)

class ContactRequest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    sender_username = db.Column(db.String(64))
    receiver_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    receiver_username = db.Column(db.String(64))
    timestamp = db.Column(db.DateTime, default=datetime.utcnow())
    mutual_status = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<ContactRequest {} (id {}) to {} (id {}). Status: {}>'.format(self.sender_username, 
                                                                              self.sender_id,
                                                                              self.receiver_username,
                                                                              self.receiver_id,
                                                                              self.mutual_status)
        
    def delete_request(self, _sender_username, _receiver_username):
        ContactRequest.query.filter_by(sender_username=_sender_username, receiver_username=_receiver_username).first_or_404().delete()
        db.session.commit()
        return
        
